<?php
namespace App\Controller;
use App\Entity\Station;
use App\Repository\StationRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class StationController extends AbstractController
{

    /**
    * @Route("/api/station", name="api_get_station", methods={GET})
    */

    public function getStation(StationRepository $stationRepository, SerializerInterface $serializer){

        return $this->json($stationRepository->findAll(), 200);
    }

    /**
    * @Route("/api/station", name="api_post_station", methods={POST})
    */

    public function postStation(Request $request, SerializerInterface $serializer, EntityManagerInterface $em){

        //TODO try exceptions, validator
        $jsonRequest = $request->getContent();

        $post = $serializer->deserialize($jsonRequest, Station::class, 'json');

        $em->persist($post);
        $em->flush();

        return $this->json($post, 201);
    }


}
