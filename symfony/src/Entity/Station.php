<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Hateoas\Configuration\Annotation as Hateoas;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StationRepository")
 * @ApiResource
 */
class Station
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

     /**
     * @ORM\Column(type="string", length=255)
     */
    private $adress;

     /**
     * @ORM\Column(type="string", length=255)
     */
    private $GPSposition; //TODO

     /**
     * @ORM\Column(type="integer")
     */
    private $capacity;

     /**
     * @ORM\Column(type="integer")
     */
    private $bikes;

     /**
     *@ORM\Column(type="boolean")
     */
    private $isActivate;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAdress(): ?string
    {
        return $this->Adress;
    }

    public function setAdress(string $adress): self
    {
        $this->adress = $adress;

        return $this;
    }

    public function getCapacity(): ?int
    {
        return $this->capacity;
    }

    public function setCapacity(?int $capacity): self
    {
        $this->capacity = $capacity;

        return $this;
    }

    public function getBikes(): ?int
    {
        return $this->bikes;
    }

    public function setBikes(?int $bikes): self
    {
        $this->bikes = $bikes;

        return $this;
    }

    public function getIsActivate(): ?boolean
    {
        return $this->isActivate;
    }

    public function setIsActivate(?boolean $isActivate): self
    {
        $this->isActivate = $isActivate;

        return $this;
    }